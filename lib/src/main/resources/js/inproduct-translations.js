var inProduct = {

	product: null,
	translationsTable: [],
	panelVisible: false,
	strings: [],
	currentLocale: null,
//	supportedLocales:[],
	multiNodeElements: [],
	truncatedElements: [],
	lastIdx: 0,
	isAnyTranslation: false,
	isAdmin: false,

	START_MARKER: '\u26a1',
	MID_MARKER: '\uFEFF',
	END_MARKER: '\u2060',
	LOCAL_MARKER: '\u200C',
	DATAMARKER: 'DATAMARKER',
	SINGLEMSGPOPUPDATAMARKER: 'SINGLEMSGPOPUPDATAMARKER',

	// not used. Using owasp-java-html-sanitizer in JiraCachingTranslationManager when saving translation. If you put alert() on whatever in message text,
	// it will execute on you (but you just entered it, so you are supposed to know what's coming at you), but will not propagate to the server side.
	// This is harmless, because evil does not spread. Or so I think anyway
	escapeHtml: function (unsafe) {
		return unsafe
			.replace(/&/g, "&amp;")
			.replace(/</g, "&lt;")
			.replace(/>/g, "&gt;")
			.replace(/"/g, "&quot;")
			.replace(/'/g, "&#039;");
	},

	removePopup: function (popup, blanketZIndex) {
		popup.hide();
		popup.remove();
		AJS.$('.aui-blanket').css({'z-index': blanketZIndex});
	},

	questionDialog: function (text, callback) {
		var blanketZIndex = 2500; // taken from CSS
		var me = this;
		AJS.$('#inproduct-question-dialog').remove();
		var popup = new AJS.Dialog({ width: 600, height: 200, id: 'inproduct-question-dialog'});
		popup.addHeader(me.strings['com.atlassian.translation.inproduct.question']);
		popup.addPanel("Panel 1", "panel1");
		popup.getCurrentPanel().html(text);
		popup.addButton(me.strings['com.atlassian.translation.inproduct.yes'], function (dialog) {
			me.removePopup(popup, blanketZIndex);
			if (callback) callback();
		});

		popup.addLink(me.strings['com.atlassian.translation.inproduct.no'], function (dialog) {
			me.removePopup(popup, blanketZIndex);
		});

		AJS.bind("show.dialog", function (e, data) {
			if (data.dialog.id == 'inproduct-question-dialog') {
				AJS.$('#inproduct-question-dialog').css({'z-index': '200001'});
				var blanketZIndex = AJS.$('.aui-blanket').css('z-index');
				AJS.$('.aui-blanket').css({'z-index': '200000'})
			}
		});

		popup.show();

		// moar evil. Shouldn't AUI do this all by itself?
		AJS.$('#inproduct-question-dialog').find('button').addClass('aui-button');
	},

	deleteSingleTranslationn: function (e) {
		e.stopPropagation();
		e.preventDefault();

		var me = inProduct;

		me.questionDialog(me.strings['com.atlassian.translation.inproduct.do.you.want.delete'], function () {
			var node = AJS.$(e.currentTarget);
			var key = e.currentTarget.id.split('_')[1];
			var locale = me.currentLocale.name;

			var data = {locale: encodeURIComponent(locale)};
			var endpoint = contextPath + '/rest/com.atlassian.translations.inproduct/1.0/translate/original_message/' + key + '/delete?_dummy=' + new Date().getTime();
			AJS.safeAjax({
				type: "POST",
				url: endpoint,
				data: data,
				success: function (data, status, xhr) {
					if (data.message) {
						node.attr('class', '');
						node.attr('title', '');
						node.prev().attr('class', 'translatedmessagevalue');
						node.prev().html(data.message);

						var success = me.strings['com.atlassian.translation.inproduct.delete.successful.refresh'];
						success = success.replace('{0}', 'onClick="javascript: window.location.reload(true);"');

						me.setStatus(success, false, true);

						// hide 'delete all' button if no translations left
						if (!data.isAnyTranslation) {
							AJS.$('#delete-all-translations').css('display', 'none');
						}
						me.isAnyTranslation = data.isAnyTranslation;

						// update the main translation table
						delete me.translationsTable[key];

					} else if (data.error) {
						me.setStatus(data.error, true, true);
					} else {
						me.setStatus(me.strings['com.atlassian.translation.inproduct.unknown.server.response'], true, true);
					}
				},
				error: function (xhr, status, error) {
					me.setStatus(xhr.status + ' ' + xhr.statusText, true, true);
				}
			});
		});
	},

	saveSizeAndPosition: function () {
		var dlg = '#inproduct-translation-dialog';
		var w = AJS.$(dlg).width();
		var h = AJS.$(dlg).height();
		var x = AJS.$(dlg).position().left;
		var y = AJS.$(dlg).position().top;

		AJS.$.cookie('inproducttranslate_x', x, {path: '/'});
		AJS.$.cookie('inproducttranslate_y', y, {path: '/'});
		AJS.$.cookie('inproducttranslate_w', w, {path: '/'});
		AJS.$.cookie('inproducttranslate_h', h, {path: '/'});
	},

	initToast: function () {
		AJS.$().toastmessage({
			text: '',
			sticky: false,
			position: 'middle-center',
			type: 'success',
			close: null
		});
	},

	setStatus: function (message, isError, showToast) {
		var statusBar = AJS.$('#translation-dialog-statusbar');

		statusBar.html(message);

		if (isError) {
			statusBar.css("color", "#D04437");

			if (showToast) {
				this.initToast();
				AJS.$().toastmessage('showErrorToast', message);
			}
		} else {
			statusBar.css("color", "#707070");

			if (showToast) {
				this.initToast();
				AJS.$().toastmessage('showSuccessToast', message);
			}
		}
	},

	getSizeAndPosition: function () {
		var dlg = '#inproduct-translation-dialog';
		AJS.$(dlg).css({'left': AJS.$.cookie('inproducttranslate_x') + 'px'});
		AJS.$(dlg).css({'top': AJS.$.cookie('inproducttranslate_y') + 'px'});
		AJS.$(dlg).css({'width': AJS.$.cookie('inproducttranslate_w') + 'px'});
		AJS.$(dlg).css({'height': AJS.$.cookie('inproducttranslate_h') + 'px'});
	},

	openTranslationPanelForOneOf: function (idxtable) {
		for (var i = 0; i < idxtable.length; ++i) {
//            console.log('trying to open ' + idxtable[i]);
			if (this.openTranslationPanel(parseInt(idxtable[i]))) {
				AJS.$('#show-all-wrapper').addClass('translate-div-hidden');
				AJS.$('.translated-message-searcher-wrapper').addClass('translate-div-hidden');
				return;
			}
		}
	},

	openTranslationPanel: function (index) {

		var me = this;

		for (var key in this.translationsTable) {
			if (this.translationsTable[key].index && this.translationsTable[key].index.indexOf(index) >= 0) {
				if (this.translationsTable[key].truncated) {
					return true;
				}
//				this.setStatus('Please provide translation xxxx', false, true);
//				AJS.$('.translation-panel').slideDown(function () {
				AJS.$('.translation-panel').show();
				AJS.$('.translation-panel').animate({
						width: '100%'
					}, 'fast', function () {
//					    AJS.$('.translations-table').css({'margin-bottom':'' + (AJS.$(this).height() + 20) + 'px'});
						AJS.$('.translations-table').hide();
						AJS.$('.translationbuttonswrap').show();
					}
				);
				AJS.$('#translatedkey').html(key);

				// hack to get rid of awful margin at the bottom of the dialog
				AJS.$('.ui-resizable-s').css('height', '0px');

				var endpoint = contextPath + '/rest/com.atlassian.translations.inproduct/1.0/translate/original_message/' + key + '/get' + '?_dummy=' + new Date().getTime();
				AJS.$.ajax({
					type: "GET",
					url: endpoint,
					success: function (data, status, xhr) {

						me.setStatus(me.strings['com.atlassian.translation.inproduct.provide.translation'], false, false);

						AJS.$('#submittranslation').css('display', 'none');

						if (data.translation) {
							AJS.$('#translatedmessageedit').val(data.translation);
						} else {
							AJS.$('#translatedmessageedit').val(data.message);
						}

						var trimmed_message = data.message;

						AJS.$('#originalmessage').html(trimmed_message);
						AJS.$('#originalmessage').attr('title', data.message);
						AJS.$('#translatedmessageedit').focus();
						AJS.$('#translatedmessageedit').attr('original_message', data.message);
					},
					error: function (xhr, status, error) {
						console.log(xhr);
					}
				});
				return true;
			}
		}
		return false;
	},

	highlightTranslatedElements: function (idxtable) {
		this.unhighlightTranslatedElement(true);
		for (var i = 0; i < idxtable.length; ++i) {
			this.highlightTranslatedElement([parseInt(idxtable[i])], true);
		}
	},

	createTranslationPopupId: function (indexes) {
		if (!indexes) {
			return 'unknown';
		}

		var id = '';
		for (var i = 0; i < indexes.length; ++i) {
			id += indexes[i] + '_'
		}
		return id.length > 0 ? id.substring(0, id.length - 1) : '';
	},

	showIndividualTranslationPopup: function (keysAndIdxes) {
		var keys = [];
		var unique = function (arr) {
			return arr.filter(function (el, i) {
				return arr.indexOf(el, i + 1) === -1;
			});
		};
		for (var key in keysAndIdxes) {
			keys.push(keysAndIdxes[key]);
		}
		this.filterTableByKeys(unique(keys));
	},

	popupTimer: null,

	showOnHoverPopup: function (indexes, o) {
		clearTimeout(this.popupTimer);
		var me = this;
		this.popupTimer = setTimeout(function () {
			var keysAndIdxes = [];
			var txt = '<div class="translatepopupids">';
			for (var i = 0; i < indexes.length; ++i) {
				for (var key in me.translationsTable) {
					if (me.translationsTable[key].index && me.translationsTable[key].index.indexOf(indexes[i]) >= 0) {
						keysAndIdxes[indexes[i]] = key;
						txt += indexes[i] + ': ' + key + '<br>';
					}
				}
			}
			txt += '</div>';
//            var toRight = AJS.$(window).width() - AJS.$(o).offset().left < 330;
//            var x = toRight ? AJS.$(window).width() - 330 : AJS.$(o).offset().left + 30;
			var x = AJS.$(o).offset().left;
			var toTop = AJS.$(o).offset().top > 100;
			var y = AJS.$(o).offset().top + (toTop ? -50 : 40);

			var id = me.createTranslationPopupId(indexes);
			AJS.$('#translatepopup_' + id).remove();
			var arrow = (toTop ? 'arrow_box_bottom' : 'arrow_box_top');
//            console.log(arrow);
			AJS.$('body').append('<div id="translatepopup_' + id + '" class="translatepopup ' + arrow + '">'
//            + id + '<br>'
				+ '<input type="button" value="' + me.strings['com.atlassian.translation.inproduct.click.to.translate'] + '">'
				+ txt
//            + '<br>' + x + ', ' + y
				+ '</div>');
			AJS.$('#translatepopup_' + id).css({ 'left': x + 'px', 'top': y + 'px' });
			AJS.$('#translatepopup_' + id).fadeIn();
			AJS.$('#translatepopup_' + id).data(me.SINGLEMSGPOPUPDATAMARKER, keysAndIdxes);
		}, 500);
	},

	addTranslatedElementOverClass: function (o, indexes, withPopupHover) {
		var data = AJS.$(o).data(this.DATAMARKER);
		if (data == null) {
//            console.log('no "translatebolt" data for ' + o);
		} else if (AJS.$(o).parents('#inproduct-translation-dialog').length > 0) {
//            return;
		} else {
			for (var i = 0; i < indexes.length; ++i) {
				if (data.indexOf(indexes[i]) >= 0) {
					AJS.$(o).addClass('translatedelementover');
					if (withPopupHover) this.showOnHoverPopup(indexes, o);
				}
			}
		}
	},

	highlightTranslatedElement: function (indexes, nounhightlight, withPopupHover) {
		if (AJS.$('#translatepopup_' + this.createTranslationPopupId(indexes)).length > 0) return;

		if (!nounhightlight) this.unhighlightTranslatedElement();

		if (indexes == null) return;

		var me = this;

		AJS.$('.translatedelement').each(function (i, o) {
			me.addTranslatedElementOverClass(o, indexes, withPopupHover);
		});
	},

	unhighlightTranslatedElement: function (immediate) {
		AJS.$('.translatedelement').removeClass('translatedelementover');
		clearTimeout(this.popupTimer);
		if (immediate) AJS.$('.translatepopup').remove();
		else AJS.$('.translatepopup').fadeOut('slow', function () {
			AJS.$(this).remove();
		});
	},

	hideTranslationEditor: function () {
		AJS.$('.translationbuttonswrap').hide();
		this.setStatus(this.strings['com.atlassian.translation.inproduct.click.message.to.translate'], false, false);
//		AJS.$('.translation-panel').slideUp(function () {
		AJS.$('.translation-panel').animate(
			{
				width: '1%'
			}, 'fast', function () {
				AJS.$('.translations-table').show();
				AJS.$('#show-all-wrapper').removeClass('translate-div-hidden');
				AJS.$('.translated-message-searcher-wrapper').removeClass('translate-div-hidden');
				AJS.$('.translation-panel').hide();
			}
		);
	},

	pruneMarkers: function (html) {
		if (!html) return "";
		if (html.indexOf('>') < 0) return html;
		return html.substring(html.indexOf('>') + 1)
	},

	bySortedValue: function (obj, callback, context) {
		var tuples = [];
		var me = this;

		for (var key in obj) tuples.push([key, obj[key]]);

		tuples.sort(function (a, b) {
			var a = me.pruneMarkers(a[1].message).toLowerCase();
			var b = me.pruneMarkers(b[1].message).toLowerCase();
			return a < b ? 1 : a > b ? -1 : 0
		});

		var length = tuples.length;
		while (length--) callback.call(context, tuples[length][0], tuples[length][1]);
	},

	prevText: '',

	loadTranslationsFromTable: function () {
//        var txt = '<table cellpadding="0" style="width: 100%;">';
		var txt = '<table class="translation-table">';
		var comparetxt = '';
		var me = this;

		var zebra = false;

		var messageClassOriginal = 'translatedmessagevalue';

		this.bySortedValue(this.translationsTable, function (key, value) {
			if (!value || !value.message) return;
			if (value.message.indexOf(me.START_MARKER) >= 0) {
//                console.log('skipping message with lightning bolt: ' + key + ': ' + value.message);
				return;
			}

			var maybetruncated = value.truncated ? ' truncatedtranslationkey' : '';

			var zebrastrip = zebra ? ' translationrowzebra' : '';
			zebra = !zebra;

			var deleteClass = '';
			var deleteTitle = '';
			var messageClass = messageClassOriginal;

			if (value.local) {
				deleteClass = 'deletetranslation';//icon icon-delete';
				deleteTitle = me.strings['com.atlassian.translation.inproduct.delete'];
				messageClass = 'translatedmessagevalue localtranslation';
			}

			txt += '<tr class="translationrow' + zebrastrip + '" id="translationrow_' + value.index + '">'
				+ '<td class="translatedmessagekey' + maybetruncated + '">' + key + '</td>'
//				+ '<td class="' + messageClass + '">' + me.escapeHtml(value.message) + '</td>'
				+ '<td class="' + messageClass + '">' + value.message + '</td>';
			if (value.local) {
				txt += '<td><a href="#" title="' + deleteTitle + '" class="deletetranslation" id="deletetranslation_' + key + '">' + me.strings['com.atlassian.translation.inproduct.clear'] + '</a></td>';
			} else {
				txt += '<td></td>';
			}
			txt += '<td class="translatedmessagearrow"></td></tr>';

			// todo make sure compare txt works
			comparetxt += '<tr class="translationrow"><td class="translatedmessagekey' + maybetruncated + '">' + key + '</td><td class="translatedmessagevalue' + maybetruncated + '">' + value.message + '</td></tr>\n'
		});

		txt += '</table>';

		if (me.prevText == comparetxt) {
//            console.log('same text');
		} else {
			me.prevText = comparetxt;

			AJS.$('#inproduct-translation-dialog .translations-table').html(txt);

			AJS.$('.deletetranslation').unbind('click');
			AJS.$('.deletetranslation').click(this.deleteSingleTranslationn);

			AJS.$('.translationrow').unbind('mouseover');
			AJS.$('.translationrow').mouseover(function (e) {
				AJS.$('.translationrow').removeClass('translationtableover');
				AJS.$(e.currentTarget).addClass('translationtableover');
				var indexes = e.currentTarget.id.split('_')[1];
				var idxtable = indexes.split(',');
				me.highlightTranslatedElements(idxtable);
			});
			AJS.$('#inproduct-translation-dialog').unbind('mouseout');
			AJS.$('#inproduct-translation-dialog').mouseout(function (e) {
				AJS.$('.translationrow').removeClass('translationtableover');
				AJS.$('.translatedelement').removeClass('translatedelementover');
			});
			AJS.$('.translationrow').unbind('click');
			AJS.$('.translationrow').click(function (e) {
				var indexes = e.currentTarget.id.split('_')[1];
				var idxtable = indexes.split(',');
				me.openTranslationPanelForOneOf(idxtable);
			});
			AJS.$('#canceltranslation').unbind('click');
			AJS.$('#canceltranslation').click(function (e) {
				e.preventDefault();
				me.hideTranslationEditor();
			});
			AJS.$('#translatedmessageedit').unbind('keyup');
			AJS.$('#translatedmessageedit').keyup(function () {

				var key = AJS.$('#translatedkey').html();
				var translation = this.value;

				if (this.value != this.lastValue) {
					if (this.timer) {
						clearTimeout(this.timer);
					}
					this.timer = setTimeout(function () {
						var data =
							{original: encodeURIComponent(AJS.$('#translatedmessageedit').attr('original_message')),
							translation: encodeURIComponent(AJS.$('#translatedmessageedit').val())};

						var endpoint = contextPath + '/rest/com.atlassian.translations.inproduct/1.0/translate/original_message/validate?_dummy=' + new Date().getTime();
						AJS.safeAjax({
							type: "POST",
							url: endpoint,
							data: data,
							success: function (data, status, xhr) {
								if (data.error) {
									me.setStatus(data.error, true, false);
									AJS.$('#submittranslation').css('display', 'none');
								} else {
									if (translation == me.translationsTable[key].message) {
										AJS.$('#submittranslation').css('display', 'none');
										me.setStatus(me.strings['com.atlassian.translation.inproduct.provide.translation'], false, false);
									} else {
										AJS.$('#submittranslation').css('display', 'inline');
										me.setStatus(me.strings['com.atlassian.translation.inproduct.click.save.translation'], false, false);
									}
								}
							},
							error: function (xhr, status, error) {
								console.log(xhr);
							}
						});
					}, 200);
					this.lastValue = this.value;
				}
			});
			AJS.$('#submittranslation').unbind('click');
			AJS.$('#submittranslation').click(function (e) {

				var key = AJS.$('#translatedkey').html();
				var node = AJS.$('.translations-table td[key="' + key + '"]');
				var freshTranslation = AJS.$('#translatedmessageedit').val();
				var locale = me.currentLocale.name;//AJS.$('#locales-dropdown').val();
				var data =
					{original: AJS.$('#translatedmessageedit').attr('original_message'), 
					 translation: freshTranslation,
					 locale: locale};
				var endpoint = contextPath + '/rest/com.atlassian.translations.inproduct/1.0/translate/original_message/' + key + '/save?_dummy=' + new Date().getTime();
				AJS.safeAjax({
					type: "POST",
					url: endpoint,
					data: data,
					success: function (data, status, xhr) {
						if (data.error) {
							me.setStatus(me.strings['com.atlassian.translation.inproduct.cannot.save.validation.failed'] + ' ' + data.error, true, true);
						} else {
							var success = me.strings['com.atlassian.translation.inproduct.translation.saved.successfully'];
							success = success.replace('{0}', 'onClick="javascript: window.location.reload(true);"');
							me.setStatus(success, false, true);

							AJS.$('#delete-all-translations').css('display', 'inline-block');

							// update the main translation table
							me.translationsTable[key].message = freshTranslation;
							me.translationsTable[key].local = true;

							// update main table
							node.attr('class', 'icon icon-delete');
							node.attr('title', me.strings['com.atlassian.translation.inproduct.delete']);
							node.prev().addClass('local');
							node.prev().html(freshTranslation);
							AJS.$('.deletetranslation').unbind('click');
							AJS.$('.deletetranslation').click(this.deleteSingleTranslationn);

							// hide translation panel
							me.hideTranslationEditor();
						}
					},
					error: function (xhr, status, error) {
						console.log(xhr);
						me.setStatus(me.strings['com.atlassian.translation.inproduct.error.saving'], true, true);
					}
				});
			});

			var text = AJS.$('#translated-message-searcher').searcher('getText');
			me.filterTable(text);
			me.addMouseOver();
			this.setStatus(this.strings['com.atlassian.translation.inproduct.click.message.to.translate'], false, false);
		}
		this.domModifedEnabled = true;
	},

	forEachTextNode: function (callback) {
		jQuery('*', 'body').andSelf().contents()
			.filter(function () {
				return this.nodeType === 3 || this.nodeType === 1;
			})
			.filter(function () {
				var textNodeWithText = this.nodeType === 3 && this.nodeValue != null && this.nodeValue.length > 0;
				var nonTextNodeWithTitle = this.nodeType === 1 && this.title != null && this.title.length > 0;
				var aWithText = this.nodeType === 1 && this.href != null && this.href.length > 0 && this.innerHTML != null && this.innerHTML.length > 0;
				var hasVal = this.nodeType === 1 && AJS.$(this).val() != null && AJS.$(this).val().length > 0;

				return textNodeWithText || nonTextNodeWithTitle || aWithText || hasVal;
			})
			.each(function () {
				callback(this)
			});
		jQuery('iframe').contents().find('body').find('*').contents()
			.filter(function () {
				return this.nodeType === 3 || this.nodeType === 1;
			})
			.filter(function () {
				var textNodeWithText = this.nodeType === 3 && this.nodeValue != null && this.nodeValue.length > 0;
				var nonTextNodeWithTitle = this.nodeType === 1 && this.title != null && this.title.length > 0;
				var aWithText = this.nodeType === 1 && this.href != null && this.href.length > 0 && this.innerHTML != null && this.innerHTML.length > 0;
				var hasVal = this.nodeType === 1 && AJS.$(this).val() != null && AJS.$(this).val().length > 0;

				return textNodeWithText || nonTextNodeWithTitle || aWithText || hasVal;
			})
			.each(function () {
				callback(this)
			});
	},

	scanPartOfNode: function (string, callback) {
		++this.lastIdx;

		var msg = string.substring(1);

		// check if translation is provided by local storage (translated with the plugin)
		var isLocal = msg.indexOf(this.LOCAL_MARKER) >= 0;

		if (isLocal) {
			msg = msg.substring(1);
		}

		var split = msg.split(this.MID_MARKER);
		callback(split[0], this.lastIdx);

		if (this.translationsTable[split[1]]) {
			this.translationsTable[split[1]].index.push(this.lastIdx);
		} else {
			this.translationsTable[split[1]] = { message: split[0], index: [this.lastIdx], local: isLocal };
		}
	},

	scanNode: function (string, callback, notfound) {
		var foundStartBoltButNotEndBolt = false;
		var current = string;
		var start;
		var end;
		var post = 0;
		var allText = '';
		var indexes = [];
		while (current != null) {
			start = current.indexOf(this.START_MARKER);
			end = current.indexOf(this.END_MARKER);
			if (start >= 0 && end >= 0 && end > start + 3) {
				var pre = current.substring(0, start);
				post += end;
				this.scanPartOfNode(current.substring(start, end), function (text, index) {
					allText += pre
//                        + '!'
						+ text
//                        + '¡'
					;
					indexes.push(index);
				});
				if (current.length >= end - start) {
					current = current.substring(end + 1);
				} else {
					current = null;
				}
			} else {
				if (start >= 0) {
					foundStartBoltButNotEndBolt = true;
				}
				current = null;
			}
		}
		if (allText.length > 0) {
			callback(allText + string.substring(post + 1), indexes);
		}
		if (foundStartBoltButNotEndBolt && notfound) {
			notfound();
		}
	},

	addIndexData: function (o, indexes) {
		if (o.data) {
			var data = o.data(this.DATAMARKER);
			if (!data) data = [];
			data = data.concat(indexes);
			o.data(this.DATAMARKER, data);
		}
	},

	scanNodeText: function (node) {
		var me = this;
		this.scanNode(node.nodeValue, function (str, indexes) {
			me.addIndexData(AJS.$(node).parent(), indexes);
			AJS.$(node).addClass('translatedelement');
			AJS.$(node).parent().addClass('translatedelement');

			node.nodeValue = str;// + '_' + idx;

		}, function () {
			if (node.nodeValue.indexOf(me.START_MARKER) >= 0) {
				me.multiNodeElements.push(AJS.$(node).parent());
			}
		});
	},

	scanNodeTitle: function (node, idx) {
		var me = this;
		this.scanNode(node.title, function (str, indexes) {
			me.addIndexData(AJS.$(node), indexes);
			AJS.$(node).addClass('translatedelement');
			node.setAttribute('title', str);
		});
	},

	scanNodeName: function (node) {
		var name = AJS.$(node).attr('name');
		if (!name) return;
		var me = this;
		this.scanNode(name, function (str, indexes) {
			me.addIndexData(AJS.$(node), indexes);
			AJS.$(node).addClass('translatedelement');
			AJS.$(node).attr('name', str);
		});
	},

	scanNodeValue: function (node) {
		var val = AJS.$(node).val();
		var me = this;
		if (!val) return;
		this.scanNode(val, function (str, indexes) {
			me.addIndexData(AJS.$(node), indexes);
			AJS.$(node).addClass('translatedelement');
//            console.log(AJS.$(node).val());
			if (location.href.indexOf('/upm') < 0 || AJS.$(node).val().indexOf('upm.filter.filterPlugins') < 0) {
				AJS.$(node).val(str);
			}
		});
	},

//	scanANodeInnerHtml:function (node) {
//        var me = this;
//		this.scanNode(node.innerHTML, function (str, indexes) {
//            if (node.id == 'comment-issue') {
//                console.log('scanANodeInnerHtml');
//            }
//            AJS.$(node).data(me.DATAMARKER, indexes);
//			AJS.$(node).addClass('translatedelement');
//            console.log(AJS.$(node).text() + ' ' + str);
//            console.log(node);
//            AJS.$(node).text(str);
//		});
//	},

	replaceTruncationWithGuessedKey: function (keyGuess) {
		var fragment = keyGuess.keyFragment;
		if (fragment.indexOf(this.NULL_KEY_FRAGMENT) < 0 && fragment.lastIndexOf('...') < Math.max(0, fragment.length - 3)) {
			fragment = fragment + '...';
		}
		var existing = this.translationsTable[fragment];
//        if (fragment.indexOf(this.NULL_KEY_FRAGMENT) == 0) {
//            console.log(fragment + ': ' + existing);
//            console.log(this.translationsTable);
//        }
		var msg = existing ? existing.message : '!undefined!';
		var local = existing ? existing.local : false;
		if (this.translationsTable[keyGuess.key]) {
			this.translationsTable[keyGuess.key].index.push(keyGuess.index);
		} else {
			this.translationsTable[keyGuess.key] = { message: msg, index: [keyGuess.index], local: local };
		}
//        console.log('deleting translation #' + keyGuess.index + ' for key: ' + fragment);
		delete this.translationsTable[fragment];
	},

	nullKeyFragmentCnt: 1,
	NULL_KEY_FRAGMENT: '***nullKeyFragment***',

	handleTruncatedMarkers: function () {
		var truncations = [];
		var me = this;
		for (var i = 0; i < this.truncatedElements.length; ++i) {
			var node = this.truncatedElements[i];
			var text = AJS.$(node).text();

			// check for local translation marker
			var isLocal = text.indexOf(this.LOCAL_MARKER) >= 0;
			var localShift = (isLocal ? 1 : 0);

			var msg = text.substring(1 + localShift);
			var split = msg.split(me.MID_MARKER);
			++me.lastIdx;
			me.addIndexData(AJS.$(node), [me.lastIdx]);
			AJS.$(node).addClass('translatedelement');
			var keyFragment = split[1] ? split[1] : this.NULL_KEY_FRAGMENT + (this.nullKeyFragmentCnt++);
			if (this.translationsTable[keyFragment]) {
				this.translationsTable[keyFragment].index.push(me.lastIdx);
			} else {
				this.translationsTable[keyFragment] = { message: split[0], index: [me.lastIdx], truncated: true, local: isLocal };
			}
			truncations.push({ key: keyFragment, message: split[0], index: me.lastIdx});
			AJS.$(node).text(split[0]);
//            console.log(split[0]);
		}
		if (truncations.length == 0) {
			me.loadTranslationsFromTable();
		} else {
			var endpoint = contextPath + '/rest/com.atlassian.translations.inproduct/1.0/translate/guesskeys?_dummy=' + new Date().getTime();
			var data = 'keysandmessages=' + encodeURIComponent(JSON.stringify(truncations));
			AJS.$.ajax({
				type: "POST",
				data: data,
				url: endpoint,
				contentType: 'application/json',
				success: function (data, status, xhr) {
					for (var i = 0; i < data.keyGuesses.length; ++i) {
						me.replaceTruncationWithGuessedKey(data.keyGuesses[i]);
					}
//                console.log(data);
					me.loadTranslationsFromTable();
				},
				error: function (xhr, status, error) {
					console.log(xhr);
				}
			});
		}
	},

	scanMultiNodeElements: function () {
		var me = this;
		for (var i = 0; i < this.multiNodeElements.length; ++i) {
			var node = this.multiNodeElements[i];
//            console.log(this.multiNodeElements[i]);

			var string = AJS.$(node).html();
			this.scanNode(string, function (str, indexes) {
				me.addIndexData(AJS.$(node), indexes);
				AJS.$(node).addClass('translatedelement');
				AJS.$(node).html(str);
			}, function () {
				if (string.lastIndexOf('...') == string.length - 3) {
					me.truncatedElements.push(node);
//                    console.log('truncted markers found in ' + string);
				} else {
//                    console.log('markers not found in ' + string);
				}
			});
		}
		me.handleTruncatedMarkers();
	},

	scanDom: function (clear) {
//        return;
		var me = this;
		this.truncatedElements = [];
		this.domModifedEnabled = false;

		if (clear) {
			this.translationsTable = [];
			this.lastIdx = 0;

			var title = AJS.$('title').text();
			this.scanNode(title, function (str, indexes) {
				var suffix = title.substring(title.lastIndexOf(this.END_MARKER));
				if (suffix.indexOf(' - ') >= 0) {
					suffix = suffix.substring(suffix.lastIndexOf(' - '));
				}
				AJS.$('title').text(str + suffix);
			});
		}

//        console.log(title);

		this.forEachTextNode(function (node) {
//			if (node.value && node.value.indexOf('common.words.signup') >= 0) {
//				console.log('found xxx:');
//				console.log(node.value);
//			}
			if (node.nodeType === 3) {
				me.scanNodeText(node);
			} else if (node.nodeType === 1) {
				me.scanNodeTitle(node);
				me.scanNodeName(node);
				me.scanNodeValue(node);
//				me.scanANodeInnerHtml(node);
			}
		});
		this.scanMultiNodeElements();
	},

	addMouseOver: function () {
		AJS.$('.translatedelement').unbind('mouseover');
		AJS.$('.translatedelement').unbind('mouseout');
		var me = this;
		AJS.$('.translatedelement').bind('mouseover', function () {
			var indexes = AJS.$(this).data(me.DATAMARKER);
			me.highlightTranslatedElement(indexes, false, true);
		});
		AJS.$('.translatedelement').bind('mouseout', function () {
			me.unhighlightTranslatedElement();
		});
//        AJS.$('iframe').contents().find('.translatedelement').mouseover(function () {
//            var indexes = AJS.$(this).data(me.DATAMARKER);
//            console.log(this);
//            console.log(indexes);
//            AJS.$(this).css({ 'background-color': 'red'});
//            me.highlightTranslatedElement(indexes);
//        });
//        AJS.$('iframe').contents().find('.translatedelement').mouseout(function () {
//            me.unhighlightTranslatedElement();
//        });
		AJS.$('.translatedmessagearrow').live('mouseover', function () {
			AJS.$(this).addClass('hover');
		});
		AJS.$('.translatedmessagearrow').live('mouseout', function () {
			AJS.$(this).removeClass('hover');
		});
		AJS.$('.translatepopup').live('mouseover', function () {
			AJS.$(this).stop().animate({opacity: '100'});
//            AJS.$(this).fadeIn('fast');
			AJS.$(this).addClass('translatepopupselected');
		});
		AJS.$('.translatepopup :button').live('mouseover', function () {
			AJS.$(this).parent().stop().animate({opacity: '100'});
//            AJS.$(this).fadeIn('fast');
			AJS.$(this).parent().addClass('translatepopupselected');
		});
		AJS.$('.translatepopup').live('mouseout', function () {
			AJS.$(this).removeClass('translatepopupselected');
			AJS.$(this).fadeOut('slow', function () {
				AJS.$(this).remove();
			});
		});
		AJS.$('.translatepopup :button').live('click', function () {
			var keysAndIdxes = AJS.$(this).parent().data(me.SINGLEMSGPOPUPDATAMARKER);
			if (!keysAndIdxes || keysAndIdxes.length == 0) return;
			AJS.$(this).parent().remove();
			me.showIndividualTranslationPopup(keysAndIdxes);
		});
	},

	getLocalesAnd: function (callback) {
		var me = this;
		var endpoint = contextPath + '/rest/com.atlassian.translations.inproduct/1.0/translate/locales?_dummy=' + new Date().getTime();
		AJS.$.ajax({
			type: "GET",
			url: endpoint,
			success: function (data, status, xhr) {
//				me.supportedLocales = data.supportedLocales;
				me.currentLocale = data.currentLocale;
				callback();
			},
			error: function (xhr, status, error) {
				console.log(xhr);
			}
		});
	},

	domModifiedTimer: null,
	domModifedEnabled: true,

	domModified: function (what) {
		clearTimeout(this.domModifiedTimer);
		var me = this;
		this.domModifiedTimer = setTimeout(function () {
			if (!me.domModifedEnabled) return;
//            console.log(what);
			me.scanDom(false);
		}, 300);
	},

	rezebra: function () {
		var z = 0;
		AJS.$('.translationrow').each(function (i, o) {
			if (AJS.$(o).hasClass('translationrowhidden')) return;
			if (z % 2) {
				AJS.$(o).addClass('translationrowzebra');
			} else {
				AJS.$(o).removeClass('translationrowzebra');
			}
			++z;
		});
	},

	filterTableByKeys: function (keys) {
		if (keys == null || keys.length == 0) {
			AJS.$('.translationrow').removeClass('translationrowhidden');
		} else {
			var selected = null;
			AJS.$('.translationrow .translatedmessagekey').each(function (i, o) {
				var key = AJS.$(o).html();
				var found = false;
				for (var j = 0; j < keys.length; ++j) {
					if (key == keys[j]) {
						AJS.$(o).parent().removeClass('translationrowhidden');
						found = true;
						selected = o;
						break;
					}
				}
				if (!found) {
					AJS.$(o).parent().addClass('translationrowhidden');
				}
			});
			if (keys.length == 1) {
				this.openTranslationPanelForOneOf(this.translationsTable[keys[0]].index);
			}
			this.showHideShowAllLink(true);
		}
		this.rezebra();
	},

	filterTable: function (text) {
		if (text == null || text.length == 0) {
			AJS.$('.translationrow').removeClass('translationrowhidden');
		} else {
			AJS.$('.translationrow .translatedmessagevalue').each(function (i, o) {
				if (AJS.$(o).html().toLowerCase().indexOf(text.toLowerCase()) >= 0) {
					AJS.$(o).parent().removeClass('translationrowhidden');
				} else {
					AJS.$(o).parent().addClass('translationrowhidden');
				}
			});
		}
		this.rezebra();
	},

	showHideShowAllLink: function (show) {
		if (show) {
			AJS.$('.translated-message-searcher-wrapper').hide();
			AJS.$('#show-all-wrapper').show();
		} else {
			AJS.$('.translated-message-searcher-wrapper').show();
			AJS.$('#show-all-wrapper').hide();
		}
	},

	setSearcherSizes: function () {
		var dw = AJS.$('#delete-all-translations').width();
		AJS.$('.translated-message-searcher-wrapper').css({'padding-right': '' + dw + 'px'});
		var ww = AJS.$('.translated-message-searcher-wrapper').width();
		AJS.$('#translated-message-searcher').css({'width': (ww - dw + 40) + 'px'});
		AJS.$('.translated-message-searcher-close').css({'right': (dw + 24) + 'px'});
	},

	addTranslationDialog: function () {
		if (AJS.$('#inproduct-translation-dialog').length > 0) return;

		var me = this;

		this.getLocalesAnd(function () {
			AJS.$('body').append(InProductTranslation.translateDialog());

			if (me.isAdmin) {
//                var analytics = inProductGa.gaEnabled
//                    ? me.strings['com.analytics.translation.inproduct.analytics.on']
//                    : me.strings['com.analytics.translation.inproduct.analytics.off'];
//                AJS.$('#analytics-warning-menu').parent().attr('title', analytics);
//
				AJS.$('#ipt-cogmenu').dropDown("Standard", {activeClass: "ipt-dropdown-active"});
				AJS.$('#ipt-cogmenu').show();
			}

			AJS.$('#inproduct-translation-dialog').resizable({
				start: function () {
					me.showIptGlass();
				},
				stop: function (e, ui) {
					me.hideIptGlass();
					me.saveSizeAndPosition();
				}
			});
			AJS.$('#inproduct-translation-dialog').resize(function () {
				me.setSearcherSizes();
			});

			AJS.$('#inproduct-translation-dialog').draggable({
				handle: '.translation-dialog-head',
				start: function () {
					me.showIptGlass();
				},
				stop: function (e, ui) {
					me.hideIptGlass();
					me.saveSizeAndPosition();
				}
			});

			var localeName = me.currentLocale.displayName;
			localeName = localeName[0].toUpperCase() + localeName.slice(1);
			var head =
				AJS.format(me.strings['com.atlassian.translation.inproduct.translate.to.language'], localeName) +
					''
//					InProductTranslation.localesDropdown({locales:me.supportedLocales, currentLocale:me.currentLocale })
				+ '<div class="rescan-message-keys-wrapper"><input type="button" id="rescan-message-keys" value="' + me.strings['com.atlassian.translation.inproduct.scannew'] + '"></div>'
//					+ '<div class="delete-all-translations-wrapper"><input type="button" id="delete-all-translations" value="' + me.strings['com.atlassian.translation.inproduct.delete.all'] + '" title="' + me.strings['com.atlassian.translation.inproduct.delete.all.title'] + '"></div>'
//					+ '<div class="delete-all-translations-wrapper icon icon-delete">sdfd</div>'
				+ '<div class="translated-message-searcher-wrapper">'
				+ '<input type="text" id="translated-message-searcher" class="formbox">'
				+ '<div id="delete-all-translations"><a href="#" title="' + me.strings['com.atlassian.translation.inproduct.delete.all.title'] + '">' + me.strings['com.atlassian.translation.inproduct.clear.all'] + '</a></div></div>'
				+ '<div id="show-all-wrapper"><a href="#">' + me.strings['com.atlassian.translation.inproduct.show.all'] + '</a></div>';
			AJS.$('#inproduct-translation-dialog .translation-dialog-head').html(head);
			AJS.$('#canceltranslation').html(me.strings['com.atlassian.translation.inproduct.cancel']);
			AJS.$('#submittranslation').val(me.strings['com.atlassian.translation.inproduct.save']);
			AJS.$('#inproduct-translation-dialog .closetranslationdialog').click(function (e) {
				e.preventDefault();
				me.toggleTranslateMode();
			});
			AJS.$('#rescan-message-keys').click(function () {
				me.scanDom(false);
			});

			if (!me.isAnyTranslation) {
				AJS.$('#delete-all-translations').css('display', 'none');
			}

			AJS.$('#delete-all-translations a').click(function () {

				me.questionDialog(me.strings['com.atlassian.translation.inproduct.do.you.want.delete.all'], function () {
					var locale = me.currentLocale.name;
					var data = {locale: encodeURIComponent(locale)};
					var endpoint = contextPath + '/rest/com.atlassian.translations.inproduct/1.0/translate/original_message/delete?_dummy=' + new Date().getTime();
					AJS.safeAjax({
						type: "POST",
						url: endpoint,
						data: data,
						success: function (data, status, xhr) {
							if (data == true) {
								me.setStatus(me.strings['com.atlassian.translation.inproduct.delete.all.successful.refresh'], false, false);
								setTimeout(function () {
									location.reload(true);
								}, 100);
							} else if (data.error) {
								me.setStatus(data.error, true, true);
							} else {
								me.setStatus(me.strings['com.atlassian.translation.inproduct.unknown.server.response'], true, true);
							}
						},
						error: function (xhr, status, error) {
							me.setStatus(xhr.status + ' ' + xhr.statusText, true, true);
						}
					});
				});
			});

			AJS.$('#translated-message-searcher').searcher({
				title: me.strings['com.atlassian.translation.inproduct.findmessage'],
				changed: function (text) {
					me.filterTable(text);
				}
			});

			me.getSizeAndPosition();
			me.scanDom(true);
			AJS.$('body').live('DOMSubtreeModified', function () {
				me.domModified(this);
			});

			AJS.$('#show-all-wrapper a').live("click", function (e) {
				e.preventDefault();
				me.showHideShowAllLink(false);
				me.filterTable(null);
				me.hideTranslationEditor();
			});

			me.setSearcherSizes();

//            me.handleGaOverlay();
//            if (!me.showGaMenu) {
//                AJS.$('#analytics-warning-menu').parent().hide();
//            }
		});
	},

//    handleGaOverlay: function() {
//        var me = this;
//        if (me.showGaMenu && !me.gaAsked) {
//            if (!me.isAdmin) {
//                AJS.$('.translationgaallowedoverlay').hide();
//            } else {
//                AJS.$('.translated-message-searcher-wrapper').hide();
//                AJS.$('#inproduct-translation-ga-allowed').click(function(e) {
//                    e.preventDefault();
//                    me.setGaAllowed(true);
//                });
//                AJS.$('#inproduct-translation-ga-not-allowed').click(function(e) {
//                    e.preventDefault();
//                    me.setGaAllowed(false);
//                });
//            }
//        } else {
//            AJS.$('.translationgaallowedoverlay').hide();
//        }
//    },

//    setGaAllowed: function(allowed) {
//        if (!this.isAdmin) return;
//        var endpoint = contextPath + '/rest/com.atlassian.translations.inproduct/1.0/translate/gaallowed?_dummy=' + new Date().getTime();
//        AJS.$.ajax({
//            type:"POST",
//            url:endpoint,
//            data: "allowed=" + allowed,
//            success:function (data, status, xhr) {
//                inProductGa.optInOut(allowed, function() {
//                    setTimeout(function() {
//                        location.reload(true);
//                    }, 1000);
//                });
//            },
//            error:function (xhr, status, error) {
//                console.log(xhr);
//            }
//        });
//    },

	addUrlParam: function (url, param) {
		if (url.indexOf('?') > 0) {
			return url + '&' + param;
		}
		return url + '?' + param;
	},

	isTranslateMode: function () {
		return AJS.$.cookie('inproducttranslatemode') == 'true'
	},

	sendTranslateModeToggle: function (callback) {
		var endpoint = contextPath + '/rest/com.atlassian.translations.inproduct/1.0/translate/toggle?_dummy=' + new Date().getTime();

		AJS.$.ajax({
			type: "POST",
			url: endpoint,
			contentType: 'application/json',
			complete: function () {
				callback();
			}
			// TODO: error handling?
		});
	},

	toggleTranslateMode: function () {
		if (this.isTranslateMode()) {
			this.sendTranslateModeToggle(function () {
				AJS.$.cookie('inproducttranslatemode', 'false', {path: '/'});
				location.reload(true);
			});
		} else {
			this.sendTranslateModeToggle(function () {
				AJS.$.cookie('inproducttranslatemode', 'true', {path: '/'});
				location.reload(true);
			});
		}
	},

	adminPages: {
		'JIRA': {
			access: '/secure/admin/jira/InProductAccessManagement.jspa',
			translations: '/secure/admin/jira/InProductTranslationsManagement.jspa'
		},
		'Confluence': {
			access: '/admin/plugins/ipt/access.action',
			translations: '/admin/plugins/ipt/translations.action'
		}
	},

	decorateFooter: function () {
		var me = this;

		var link = me.product === 'JIRA'
			? AJS.$('#inproduct-translate-dropdown-section a')
			: AJS.$('#user-menu-link-inproduct-translate-dropdown-section a');
		link.attr('href', '');
		link.unbind('click');
		link.click(function (e) {
			e.preventDefault();
			me.toggleTranslateMode();
		});

		if (me.isTranslateMode()) {
			link.html(me.strings['com.atlassian.translation.inproduct.dropdown.webitem.label.stop']);
			me.addTranslationDialog();

			AJS.$('#access-management-menu').live('click', function (e) {
				e.preventDefault();
				location = contextPath + me.adminPages[me.product].access;
			});
//            if (me.showGaMenu) {
//                AJS.$('#analytics-warning-menu').live('click', function(e) {
//                    e.preventDefault();
//                    location = contextPath + me.adminPages[me.product].ga;
//                });
//            }
			AJS.$('#translations-mgmt-menu').live('click', function (e) {
				e.preventDefault();
				location = contextPath + me.adminPages[me.product].translations;
			});
		}
	},

	handleLanguageChange: function () {
		if (this.product === 'JIRA') {
			this.handleLanguageChangeJira();
		} else {
			this.handleLanguageChangeConfluence();
		}
	},

	handleLanguageChangeJira: function () {

		var me = this;

		var profilePage = AJS.$("#up-p-locale");

//		console.log('xxx' + AJS.Meta.getBoolean('inproduct-show-translation-tip'));

		if (AJS.HelpTip && profilePage.length && AJS.Meta.getBoolean('inproduct-show-translation-tip') && !this.isTranslateMode()) {

			var newUsersTip, tipAnchor;
			var baseAnchor = AJS.$('#header-details-user-fullname');

			if (baseAnchor.length) {
				tipAnchor = AJS.$("<div></div>").css({
					"float": "right",
					"z-index": " -1",
					"height": "1px",
					"width": "1px",
					"position": "relative",
					"right": "-20px"
				}).appendTo(baseAnchor);

				if (AJS.$.browser.msie) {
					tipAnchor.css({"bottom": "-20px"});
				}

				newUsersTip = new AJS.HelpTip({
					id: 'inproduct.language.changed',
					title: this.strings['com.atlassian.translation.inproduct.language.changed.popup.title'],
//					bodyHtml:'<h1>abc body</h1>',
					body: this.strings['com.atlassian.translation.inproduct.language.changed.popup.body'],
//					url:'http://atlassian.com',
					anchor: tipAnchor
				});

				var translateLink = AJS.$('#inproduct_translate_page');
				if (translateLink.length) {
					translateLink.unbind('click');
					translateLink.click(function (e) {
						e.preventDefault();
						newUsersTip.dismiss("inProductTranslation");
						me.toggleTranslateMode();
					});
				}

//				AJS.HelpTip.Manager.undismiss(newUsersTip);
				newUsersTip.show();
			}
		}
	},

	handleLanguageChangeConfluence: function () {

		var me = this;

		var profilePage = AJS.$("#up-p-locale");

//		console.log('xxx' + AJS.Meta.getBoolean('inproduct-show-translation-tip'));

		if (!this.isTranslateMode()) {
			var translateLink = AJS.$('#inproduct_translate_page');
			if (translateLink.length) {
				translateLink.unbind('click');
				translateLink.click(function (e) {
					e.preventDefault();
					me.toggleTranslateMode();
				});
			}
		}
	},

	initIptGlass: function () {
		AJS.$('body').append(AJS.$('<div></div>').attr('id', 'ipt-glass'));
	},

	showIptGlass: function () {
		AJS.$('#ipt-glass').show();
	},

	hideIptGlass: function () {
		AJS.$('#ipt-glass').hide();
	},

	init: function (isAnyTranslation, isAdmin) {
		var me = this;
		me.initIptGlass();
		me.isAnyTranslation = isAnyTranslation;
		me.isAdmin = isAdmin;
		var endpoint = contextPath + '/rest/com.atlassian.translations.inproduct/1.0/translate/strings?_dummy=' + new Date().getTime();
		AJS.$.ajax({
			type: "GET",
			url: endpoint,
			success: function (data, status, xhr) {
				for (var i = 0; i < data.strings.length; ++i) {
					me.strings[data.strings[i].key] = data.strings[i].value;
				}
				me.decorateFooter();
				me.handleLanguageChange();
			},
			error: function (xhr, status, error) {
				console.log(xhr);
			}
		});
	}
};

AJS.$(document).ready(function () {
	var endpoint = contextPath + '/rest/com.atlassian.translations.inproduct/1.0/translate/initdata?_dummy=' + new Date().getTime();
	AJS.$.ajax({
		type: "GET",
		url: endpoint,
		success: function (data, status, xhr) {
			if (data.allowed) {
				inProduct.product = data.product;
				inProduct.init(data.isAnyTranslation, data.isAdmin);
			} else {
				AJS.$.cookie('inproducttranslatemode', 'false', {path: '/'});
			}
		},
		error: function (xhr, status, error) {
			console.log(xhr);
		}
	});
});

function checkLocale() {
	var localeNewValue = AJS.$.trim(AJS.$("#update-user-preferences-locale option:selected").text());
	var localeOldValue = AJS.$.trim(AJS.$("#up-p-locale").text());

	if (localeOldValue !== localeNewValue) {
		this._reload = function () {
			return true;
		}
	}
}

function stringToBytes(str) {
	var ch, st, re = [];
	for (var i = 0; i < str.length; i++) {
		ch = str.charCodeAt(i);  // get char
		st = [];                 // set up "stack"
		do {
			st.push(ch & 0xFF);  // push byte to stack
			ch = ch >> 8;          // shift value down by 1 byte
		}
		while (ch);
		// add stack contents to result
		// done because chars have "wrong" endianness
		re = re.concat(st.reverse());
	}
	// return an array of bytes
	return re;
}

