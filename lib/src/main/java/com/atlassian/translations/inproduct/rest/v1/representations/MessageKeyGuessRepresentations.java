package com.atlassian.translations.inproduct.rest.v1.representations;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * User: kalamon
 * Date: 03.07.12
 * Time: 10:41
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MessageKeyGuessRepresentations {
    @XmlElement private List<MessageKeyGuessRepresentation> keyGuesses = new ArrayList<MessageKeyGuessRepresentation>();

    public MessageKeyGuessRepresentations(List<MessageKeyGuessRepresentation> keyGuesses) {
        this.keyGuesses = keyGuesses;
    }

    public MessageKeyGuessRepresentations() {
    }
}
