package com.atlassian.translations.inproduct.rest.v1.representations;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * TODO: Document this class / interface here
 *
 * @since v5.0
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@XmlRootElement
public class OriginalMessageWithTranslationRepresentation {
	@XmlElement
	private String message;
	@XmlElement
	private String translation;

	public OriginalMessageWithTranslationRepresentation(String message, String translation) {
		this.message = message;
		this.translation = translation;
	}
}
