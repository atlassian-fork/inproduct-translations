package com.atlassian.translations.inproduct.components;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * User: kalamon
 * Date: 18.12.12
 * Time: 10:24
 */
public class LanguagePackDownloadServlet extends HttpServlet {
    private static final int BUFF_SIZE = 10240;

    private final LanguagePackGenerator languagePackGenerator;
    private final AccessManager accessManager;

    public LanguagePackDownloadServlet(LanguagePackGenerator languagePackGenerator, AccessManager accessManager) {
        this.languagePackGenerator = languagePackGenerator;
        this.accessManager = accessManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        boolean allowed = accessManager.isCurrentUserAllowedToTranslate();
        if (!allowed) {
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        } else {
            sendLanguagePack(req, resp);
        }
    }

    private synchronized void sendLanguagePack(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String language = req.getParameter("language");
        if (language == null) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        try {
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.setContentType("application/xml");

            File zipFile = languagePackGenerator.generate(language);

            sendFile(zipFile, resp);
        } catch (IllegalArgumentException e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    private void sendFile(File file, HttpServletResponse response) throws IOException {
        String contentType = getServletContext().getMimeType(file.getName());

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        response.reset();
        response.setBufferSize(BUFF_SIZE);
        response.setContentType(contentType);
        response.setHeader("Content-Length", String.valueOf(file.length()));
        response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

        BufferedInputStream input = null;
        BufferedOutputStream output = null;

        try {
            input = new BufferedInputStream(new FileInputStream(file), BUFF_SIZE);
            output = new BufferedOutputStream(response.getOutputStream(), BUFF_SIZE);

            byte[] buffer = new byte[BUFF_SIZE];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
        } finally {
            close(output);
            close(input);
        }
    }

    private static void close(Closeable resource) throws IOException {
        if (resource != null) {
            resource.close();
        }
    }
}
