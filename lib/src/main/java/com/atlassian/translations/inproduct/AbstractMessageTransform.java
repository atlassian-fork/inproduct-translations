package com.atlassian.translations.inproduct;

import com.atlassian.translations.inproduct.components.MessageKeyGuesser;
import com.atlassian.translations.inproduct.components.TranslationManager;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * User: kalamon
 * Date: 20.06.13
 * Time: 14:50
 */
public abstract class AbstractMessageTransform {
    public static final char START_HIGHLIGHT_CHAR = '\u26A1'; // lightning
    public static final char LOCAL_TRANSLATION_CHAR = '\u200C';	// zero width non-joiner
    public static final char MIDDLE_HIGHLIGHT_CHAR = '\uFEFF';  // BOM
    //    private static final char START_HIGHLIGHT_CHAR = '\uFEFF';  // BOM
//	private static final char MIDDLE_HIGHLIGHT_CHAR = '\u26A1'; // lightning
    protected static final char END_HIGHLIGHT_CHAR = '\u2060';	// zero width word joiner

    protected static final String TRANSLATEMODE = "inproducttranslatemode";

    private final TranslationManager translationManager;
    private final MessageKeyGuesser guesser;

    protected abstract HttpServletRequest getCurrentRequest();

    protected AbstractMessageTransform(TranslationManager translationManager, MessageKeyGuesser guesser) {
        this.translationManager = translationManager;
        this.guesser = guesser;
    }

    public String apply(Locale locale, String key, String rawMessage) {
        boolean localTranslation = true;
        String translation = findTranslation(locale, key);
        if (translation == null) {
            translation = rawMessage;
            localTranslation = false;
        } else {
//            translation = MessageTransformUtil.sanitize(translation);
        }
        if (isTranslationMode()) {
            guesser.add(locale, key, translation);

            if (localTranslation) {
                return String.format(
                        "%c%c%s%c%s%c",
                        START_HIGHLIGHT_CHAR,
                        LOCAL_TRANSLATION_CHAR,
                        translation,
                        MIDDLE_HIGHLIGHT_CHAR,
                        key,
                        END_HIGHLIGHT_CHAR);
            } else {
                return String.format(
                        "%c%s%c%s%c",
                        START_HIGHLIGHT_CHAR,
                        translation,
                        MIDDLE_HIGHLIGHT_CHAR,
                        key,
                        END_HIGHLIGHT_CHAR);
            }
        }
        return translation;
    }

    private String findTranslation(Locale locale, String key) {
        return translationManager.getTranslation(locale.toString(), key);
    }

    private boolean isTranslationMode() {
        HttpServletRequest request = getCurrentRequest();
        if (request == null) {
            return false;
        }
        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            return false;
        }
        for (Cookie c : cookies) {
            if (c.getName().equals(TRANSLATEMODE)) {
                return Boolean.parseBoolean(c.getValue());
            }
        }
        return false;
    }
}
