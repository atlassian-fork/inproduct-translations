package com.atlassian.translations.inproduct.actions;

import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.translations.inproduct.components.TranslationManager;
import com.google.common.base.Objects;

import java.util.Locale;
import java.util.Map;

/**
 * User: kalamon
 * Date: 25.06.13
 * Time: 15:22
 */
public class ConfluenceTranslationsManagementAction extends AbstractIptAction {
    protected final TranslationManager translationManager;
    private final LocaleManager localeManager;

    public ConfluenceTranslationsManagementAction(
            TranslationManager translationManager, LocaleManager localeManager, WebResourceManager webResourceManager) {
        super(webResourceManager);
        this.translationManager = translationManager;
        this.localeManager = localeManager;
    }

    public Locale getCurrentLocale() {
        return localeManager.getLocale(AuthenticatedUserThreadLocal.get());
    }

    public Map<String, String> getTranslations() {
        return translationManager.getAllTranslations(getCurrentLocale().toString());
    }

    public String getName() {
        return "translations-save.action";
    }

    @Override
    public String execute() throws Exception {
        String res = super.execute();
        if (Objects.equal(SUCCESS, res)) {
            if (xsrfOk()) {
                String keytodelete = getRequestParameter("keytodelete");
                translationManager.deleteTranslation(getCurrentLocale().toString(), keytodelete);
            } else {
                return ERROR;
            }
        }
        return res;
    }
}
