package com.atlassian.translations.inproduct.components;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.core.task.Task;
import com.atlassian.translations.inproduct.ao.AoTranslationsManager;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.concurrent.Callable;

/**
 * User: kalamon
 * Date: 10.07.12
 * Time: 15:08
 */
public class ConfluenceCachingTranslationManager extends AbstractCachingTranslationManager {
    private static final Logger LOG = Logger.getLogger(ConfluenceCachingTranslationManager.class);

    private final MultiQueueTaskManager taskManager;

    private static ConfluenceCachingTranslationManager instance;

    public static ConfluenceCachingTranslationManager getInstance() {
        return instance;
    }

    public ConfluenceCachingTranslationManager(
            LocaleManager localeManager, I18NBeanFactory i18NBeanFactory,
            MultiQueueTaskManager taskManager,
            ActiveObjects ao) {
        super(new AoTranslationsManager(localeManager, i18NBeanFactory, ao));
        this.taskManager = taskManager;
        instance = this;
    }

    @Override
    protected void runTask(final Callable<Serializable> task, String message) {
        MySuperTask taskToExecute = new MySuperTask(task);
        taskManager.addTask("task", taskToExecute);
        taskManager.flush("task");
    }

    @Override
    void loadAoToCache() {
        runTask(new MySuperCallable(), "MessageCache initialization");
    }
}