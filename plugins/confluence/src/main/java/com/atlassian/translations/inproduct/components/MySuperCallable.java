package com.atlassian.translations.inproduct.components;

import java.io.Serializable;
import java.util.concurrent.Callable;

/**
 * Created by mfedoryshyn on 20/05/2016.
 */
public class MySuperCallable implements Callable<Serializable>, Serializable {

    @Override
    public Serializable call() throws Exception {
        if (ConfluenceCachingTranslationManager.getInstance() != null) {
            ConfluenceCachingTranslationManager.getInstance().initializeCache();
        }
        return null;
    }
}
