package com.atlassian.translations.inproduct.components;

import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.translations.inproduct.ao.AoAllowedGroupsManager;
import com.atlassian.user.EntityException;
import com.atlassian.user.Group;
import com.atlassian.user.GroupManager;
import com.atlassian.user.User;
import com.atlassian.user.search.page.PagerUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * User: kalamon
 * Date: 17.07.12
 * Time: 11:01
 *
 */
public class ConfluenceAccessManager implements AccessManager {
    private static final Logger LOG = Logger.getLogger(ConfluenceAccessManager.class);

    private final GroupManager groupManager;
    private final AoAllowedGroupsManager allowedGroupsManager;

    public ConfluenceAccessManager(final GroupManager groupManager, final AoAllowedGroupsManager allowedGroupsManager) {
        this.groupManager = groupManager;
        this.allowedGroupsManager = allowedGroupsManager;
    }

    public static class GroupSpec {
        private boolean allowed;
        private String groupName;

        public GroupSpec(String groupName, boolean allowed) {
            this.groupName = groupName;
            this.allowed = allowed;
        }

        public boolean isAllowed() {
            return allowed;
        }

        public String getGroupName() {
            return groupName;
        }
    }

    public List<GroupSpec> getGroups() {
        Collection<String> allowedGroups = allowedGroupsManager.getAllowedGroups();
        boolean all = allowedGroups == null;
        List<GroupSpec> result = new ArrayList<GroupSpec>();
        Collection<Group> groups = null;
        try {
            groups = PagerUtils.toList(groupManager.getGroups());
            for (Group group : groups) {
                result.add(new GroupSpec(group.getName(), all || allowedGroups.contains(group.getName())));
            }
        } catch (EntityException e) {
            LOG.warn(e.getMessage(), e);
        }
        return result;
    }

    public void setGroups(String[] groups) {
        if (groups == null) {
            allowedGroupsManager.setAllowedGroups(null);
        } else {
            List<String> groupList = Arrays.asList(groups);
            allowedGroupsManager.setAllowedGroups(groupList);
        }
    }

    public boolean isUserAllowedToTranslate(User user) {
        if (user == null) {
            return false;
        }
        Collection<String> allowedGroups = allowedGroupsManager.getAllowedGroups();
        if (allowedGroups == null) {
            return true;
        }
        for (String allowedGroup : allowedGroups) {
            try {
                if (groupManager.hasMembership(groupManager.getGroup(allowedGroup), user)) {
                    return true;
                }
            } catch (EntityException e) {
                LOG.warn(e.getMessage(), e);
            }
        }
        return false;
    }

    @Override
    public boolean isCurrentUserAllowedToTranslate() {
        return isUserAllowedToTranslate(AuthenticatedUserThreadLocal.get());
    }
}
