package com.atlassian.translations.inproduct.conditions;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.atlassian.translations.inproduct.components.ConfluenceAccessManager;

import java.util.Map;

/**
 * @author jjaroczynski
 *
 * @since v5.0
 */
public class AllowedToTranslateCondition implements Condition {

	private ConfluenceAccessManager accessManager;

	public AllowedToTranslateCondition(ConfluenceAccessManager accessManager) {
		this.accessManager = accessManager;
	}

    @Override
    public void init(Map<String, String> stringStringMap) throws PluginParseException {
    }

    @Override
    public boolean shouldDisplay(Map<String, Object> stringObjectMap) {
        return accessManager.isCurrentUserAllowedToTranslate();
    }
}
