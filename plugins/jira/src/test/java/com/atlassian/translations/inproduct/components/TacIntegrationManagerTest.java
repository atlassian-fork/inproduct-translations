package com.atlassian.translations.inproduct.components;

import com.atlassian.translations.inproduct.components.TranslationsRequest.Product;
import com.atlassian.translations.inproduct.components.utils.AidLoginService;
import com.atlassian.translations.inproduct.components.utils.HttpRequestExecutor;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TacIntegrationManagerTest {

    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();

    @Mock
    private AidLoginService loginService;

    @Mock
    private HttpRequestExecutor httpRequestExecutor;

    @Mock
    private TranslationManager translationManager;

    private ArgumentCaptor<HttpPost> httpRequest;
    private Map<String, String> productMap;
    private Map<String, String> translationsMap;
    private List<Product> products;


    private TacIntegrationManager tacIntegrationManager;

    @Before
    public void setup() throws Exception{
        translationsMap = ImmutableMap.<String, String>builder()
                .put("com.atlassian.jira.helloMessage", "Hello user!")
                .put("com.atlassian.jira.goodbyeMesasge", "Goodbye")
                .build();


        productMap = ImmutableMap.<String, String>builder()
                .put("JIRA Core", "7.0.0")
                .put("JIRA Software", "7.0.0")
                .put("JIRA Service Desk", "3.0.1")
                .build();

        products = productMap.entrySet().stream()
                .map(entry -> new Product(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());


        when(loginService.login(eq("admin"), eq("admin"))).thenReturn("token");
        when(loginService.login(not(eq("admin")), not(eq("admin")))).thenThrow(new IOException("Login failed"));

        when(translationManager.getAllTranslations(Matchers.anyString())).thenReturn(translationsMap);

        httpRequest = ArgumentCaptor.forClass(HttpPost.class);
        when(httpRequestExecutor.executeRequest(Matchers.any(), httpRequest.capture(), Mockito.anyString())).thenReturn("correct");

        tacIntegrationManager = new TacIntegrationManager(
                null,
                translationManager,
                loginService,
                httpRequestExecutor);
    }


    //Publish translations section
    @Test
    public void testPublishTranslationsRequestStructure() throws Exception {
        tacIntegrationManager.publishTranslationsToTac("en_US", productMap, "admin", "admin");

        verify(httpRequestExecutor, times(1)).executeRequest(any(), any(), any());
        HttpPost post = httpRequest.getValue();

        assertEquals("Accept header is wrong", "application/json", post.getFirstHeader("Accept").getValue());
        assertEquals("Content-type header is wrong", "application/json", post.getFirstHeader("Content-Type").getValue());

        assertNotNull(post.getEntity());
        String entity = EntityUtils.toString(post.getEntity());

        TranslationsRequest translations = new Gson().fromJson(entity, TranslationsRequest.class);
        assertEquals("en_US", translations.getLanguage());

        assertNotNull(translations.getProducts());
        assertEquals(products, translations.getProducts());
        assertEquals(translationsMap, translations.getTranslations());
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testPublishTranslationsWrongLogin() throws Exception {
        expectedException.expect(IOException.class);
        expectedException.expectMessage("Login failed");
        tacIntegrationManager.publishTranslationsToTac("en_US", productMap, "Notadmin", "Notadmin");
    }
}